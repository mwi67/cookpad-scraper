import requests
import urllib.request
import time
from bs4 import BeautifulSoup
import re

#set url of cookpad
#url = "https://cookpad.com/recipe/5955291"
url = "https://cookpad.com/recipe/5998709"

#scrape recipe number
recipe_number =int(re.search(r'\d+', url).group())

#create text file title
file_title = f"{recipe_number}.txt"

#get response from cookpad website
response = requests.get(url)
#print(response)

#create soup object
soup = BeautifulSoup(response.text, "html.parser")

#get title text
title_list = soup.findAll('h1', {"class": 'recipe-title fn clearfix'})

#for element in print_list:
 #   print(element.text)

title_text = title_list[0].text + "\n"

#get ingredients list
ingredient_list = soup.findAll('div', {"class": 'ingredient_row'})

#for element in ingredient_list:
 #   print(element.text)

#get serving info
serving_info_list = soup.findAll('span', {'class': "servings_for yield"})

#get instructions list
instruction_list = soup.findAll('p', {"class": "step_text"})

#for element in instruction_list:
   # print(element.text)

#create text file
file_to_insert = open(file_title, "w+")

#write title
file_to_insert.write(f"{title_text} {url}\n\n\n")

#write serving info
for element in serving_info_list:
    element_info = element.text.replace('\n', '')
    file_to_insert.write("材料" + element_info + '\n')

#write ingredients
for element in ingredient_list:
    element_s = element.text
    file_to_insert.write(element_s)

#add space in between
file_to_insert.write('\n\n\n')

#write instructions

for index, element in enumerate(instruction_list, start=1):
    element_t = element.text.strip()
    file_to_insert.write(f"{index}. {element_t}\n\n")

file_to_insert.close()